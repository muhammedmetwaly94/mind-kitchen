$(document).ready(function () {
  const lang = document.querySelector("html").getAttribute("lang");

  $(".owl-carousel").owlCarousel({
    nav: true,
    items: 1,
    dots: true,
    margin: 10,
    responsive: {
      0: {
        items: 1,
      },
      768: {
        items: 2,
      },
      1024: {
        items: 4,
      },
    },
  });

  toastr.options = {
    closeButton: true,
    debug: false,
    newestOnTop: false,
    progressBar: true,
    closeDuration: 1000,
    positionClass: lang === "en" ? "toast-top-right" : "toast-top-left",
    preventDuplicates: true,
    onclick: null,
    showEasing: "swing",
    hideEasing: "linear",
    showMethod: "fadeIn",
    hideMethod: "fadeOut",
  };
});
